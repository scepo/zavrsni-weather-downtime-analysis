import numpy as np


def find_nearest(array, value):
    """
    Returns index of nearest value in array.

    Parameters
    ----------
    array: 2D Numpy array of floats
    value: float
    Returns
    -------
    idx: integer
        index of nearest value in array
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def kutevi(kut):
    """
    Returns angle between 0 and 360.

    Parameters
    ----------
    kut: float
    Returns
    -------
    kut:float
    """
    while kut < 0:
        kut += 360
    while kut >= 360:
        kut -= 360
    return kut


def attack_dir(ship_dir, wave_dir):
    """
    Returns angle of a wave in relation to the ship.
    Parameters
    ----------
    ship_dir: float
        direction of the ship relative to North
    wave_dir: float
        direction of a wave relative to North
    Returns
    -------
    dir: float
        direction of a wave relative to ship

    """
    dir = wave_dir - ship_dir
    dir = kutevi(dir)
    return dir


class Operation:

    def __init__(self, name, duration, direction, vaning,
                 persistence, dirs, tps, hs, meteofile_path):
        self.name = name
        self.duration, self.direction = duration, direction
        self.vaning = vaning
        self.persistence = persistence
        self.dirs = dirs
        self.tps = tps
        self.hs = hs
        self.meteofile_path = meteofile_path

    def possible(self, hs, tp, wave_dir):
        """
        Returns truth value of operation operability

        """
        # calculates direction of a wave relative to ship prefered heading
        direc = attack_dir(ship_dir=self.direction, wave_dir=wave_dir)
        # minimum and maximum values of wave direction acounting for vaning
        dir_min = kutevi(kut=direc - self.vaning)
        dir_max = kutevi(kut=direc + self.vaning)
        # hs_limit is an array with hs values that corespond to the nearest wave period given in limits and
        # all directions in between dir_min and dir_max
        hs_limit = np.array([])
        for i in range(self.dirs.shape[0]):
            if dir_min <= dir_max and self.dirs[i] > dir_min and self.dirs[i] < dir_max:
                hs_limit = np.append(hs_limit, self.hs[find_nearest(
                    self.tps, tp), find_nearest(self.dirs, self.dirs[i])])
            elif dir_min >= dir_max and self.dirs[i] < dir_min and self.dirs[i] > dir_max:
                hs_limit = np.append(hs_limit, self.hs[find_nearest(
                    self.tps, tp), find_nearest(self.dirs, self.dirs[i])])

        if hs_limit.shape[0] == 0:
            hs_limit = self.hs[find_nearest(
                self.tps, tp), find_nearest(self.dirs, direc)]

        # if maximum value of limits is higher than hs of that timestate than
        # operation is possible
        if np.max(hs_limit) > hs:
            return True
        else:
            return False


import numpy as np
