import operation
import result
import numpy as np
import matplotlib.pyplot as plt


def read_meteo_file(path, skip_header):
    dates = np.genfromtxt(
        path,
        dtype=None,
        delimiter=[4,2,2,1,2],
        usecols=(0,1,2,4),
        skip_header=skip_header,
        max_rows=4500)
    weatherdata = np.genfromtxt(
        path, dtype='float', delimiter=[
            44, 11, 11, 11], usecols=(
            1, 2, 3), skip_header=skip_header, max_rows=4500)
    return(dates, weatherdata)


def read_operationlist_file(path):
    name = np.genfromtxt(
        path,
        dtype='U25',
        skip_header=5,
        comments='!',
        usecols=0)
    meteofile_path = np.genfromtxt(
        path,
        dtype='U25',
        skip_header=5,
        comments='!',
        usecols=5)
    hs_limits_path = np.genfromtxt(
        path,
        dtype='U25',
        skip_header=5,
        comments='!',
        usecols=6)
    operation_parameters = np.genfromtxt(
        path, skip_header=5, comments='!', usecols=(1, 2, 3, 4))
    return(name, operation_parameters, meteofile_path, hs_limits_path)


def read_hslimits(path):
    limits = np.genfromtxt(path, skip_header=4)
    dirs = limits[0, 1:]
    tps = limits[1:, 0]
    hs = limits[1:, 1:]

    # transformacija limita kako bi svi bili od 0-360 stupnjeva
    if dirs[-1] == 180:
        hs2 = np.flip(hs[:, :-1], axis=1)
        hs = np.concatenate((hs, hs2), axis=1)
        dirs = np.append(dirs, 360 - np.flip(dirs, 0)[1:])
    elif dirs[-1] != 360:
        hs = np.concatenate((hs, np.array([hs[:, 0]]).T), axis=1)
        dirs = np.append(dirs, 360)

    return(dirs, tps, hs)


def find_date(dates, start, date):
    i = start
    while not np.array_equal(date, dates[i]):
        i = i + 1
    return i


def plot_durations(durations, idealduration, plot_name, x_lim, y_lim):
    fig = plt.figure()
    x = np.linspace(1, durations.shape[0], durations.shape[0])
    plt.plot(x, durations, 'o', label='Trajanje scenarija')
    plt.axhline(
        y=np.mean(durations),
        linestyle='--',
        color='g',
        label='Srednja vrijednost')
    plt.axhline(
        y=idealduration,
        linestyle='--',
        color='b',
        label='Idealno trajanje scenarija')
    plt.xlim(x_lim)
    plt.ylim(y_lim)
    plt.xlabel('Redni broj simulacije')
    plt.ylabel('Trajanje scenarija [h]')
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2))
    plt.savefig(plot_name, bbox_extra_artists=(lgd,), bbox_inches='tight')


def main():
    nebitno = 0
    operations_count = 31
    runs_number = 24
    datumi = np.array([[1984, 6, 1, 0], [1984, 7, 1, 0], [
                      1984, 8, 1, 0], [1984, 9, 1, 0], [1984, 10, 1, 0]])
    timestep = 3
    operations_path = 'vaning_30'

    operations_names, operations_parameters, meteofile_path, hs_limits_path = read_operationlist_file(
        'podaci\{operations_path}.txt'.format(operations_path=operations_path))

    operations = np.empty(operations_count, dtype=object)
    for i in range(operations_count):
        dirs, tps, hs = read_hslimits('podaci\{}'.format(hs_limits_path[i]))

        operations[i] = operation.Operation(name=operations_names[1], duration=operations_parameters[i, 1] * 24,
                                            direction=operations_parameters[i,
                                                                            0], vaning=operations_parameters[i, 3],
                                            persistence=operations_parameters[i, 2],
                                            dirs=dirs, tps=tps, hs=hs, meteofile_path=meteofile_path[i])

    # definiranje objekta za spremanje rezultata
    results = np.empty([datumi.shape[0], runs_number], dtype=object)
    durations_array = np.empty([datumi.shape[0], runs_number])

    for idx, pocetni_datum in enumerate(datumi):

        for i in range(runs_number):

            results[idx, i] = result.Result(name='Simulacija {}'.format(i + 1), startdate=np.array(pocetni_datum),
                                            enddate=np.array(pocetni_datum), idealduration=np.sum(operations_parameters[:, 1]) * 24,
                                            duration=0)
            pocetni_datum[0] += 1

        bla = 0
        start = 0
        # simulacije

        for res in results[idx]:

            dates, waves = read_meteo_file(
                'podaci\{}'.format(operations[0].meteofile_path), int(
                    360 * bla * 24 / 3))

            start = find_date(dates, 0, res.startdate) + \
                int(360 * bla * 24 / 3)
            bla += 1

            timeidx = 0

            for i in range(operations_count):
                dates, waves = read_meteo_file(
                    'podaci\{}'.format(operations[0].meteofile_path), start)

                timeworked = 0
                timespent = 0
                while timeworked < operations[i].duration:

                    if timeidx > 365 * 24 / \
                            timestep:  # time series fail ako simulacija traje duze od 1 godinu
                        timespent = 0
                        break

                    if operations[i].possible(
                            waves[timeidx, 0], waves[timeidx, 1], waves[timeidx, 2]):
                        timeworked += timestep

                    elif operations[i].persistence == 1:  # persistance
                        timeworked = 0

                    timeidx += 1

                    timespent += timestep
                if timespent == 0:  # ako time series fail vrijednost 999999
                    res.duration = 999999
                    print('error')
                    break
                res.duration += timespent
            res.enddate = dates[timeidx]
            nebitno += 1
            print(
                '{posto:.2f} %'.format(
                    posto=nebitno /
                    runs_number /
                    datumi.shape[0] *
                    100))

        for i in range(runs_number):
            durations_array[idx, i] = results[idx, i].duration
    
    for i, durations in enumerate(durations_array):

        plotname = "rezultati\{operation_file}_{dd}_{mm}".format(operation_file=operations_path,
                                                                 mm=results[i, 0].startdate[1], dd=results[i, 0].startdate[2])
        x_lim = (0, runs_number)

        y_lim = (results[0, 0].idealduration - (np.max(durations_array) - results[0, 0].idealduration) / 10,
                 np.max(durations_array) + (np.max(durations_array) - results[0, 0].idealduration) / 10)
        plot_durations(
            durations_array[i], results[0, 0].idealduration, plotname, x_lim, y_lim)

    x_percentile = np.array([0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100])
    percentile = np.empty([5, x_percentile.shape[0]])
    labels = []

    for i, durations in enumerate(durations_array):
        percentile[i, :] = np.percentile(durations, x_percentile)
        labels.append('Početak radova {dd}. {mm}.'.format(
            mm=results[i, 0].startdate[1], dd=results[i, 0].startdate[2]))

    fig1 = plt.figure()
    plt.axhline(y=4148, linestyle='--', color='b',
                label='Idealno trajanje scenarija')
    for i, durations in enumerate(durations_array):
        plt.plot(x_percentile, percentile[i], label=labels[i])
    plt.xlabel('Percentil')
    plt.ylabel('Trajanje scenarija [h]')
    plt.grid()
    plt.xlim(0, 100)
    lgd = plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2))
    plt.savefig(
        'rezultati\{operation_file}_percentili'.format(
            operation_file=operations_path), bbox_extra_artists=(
            lgd,), bbox_inches='tight')
    np.save('rezultati\{operation_file}_result_obj'.format(
        operation_file=operations_path), results)


if __name__ == '__main__':
    main()
